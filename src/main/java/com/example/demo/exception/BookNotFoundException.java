package com.example.demo.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class BookNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 3116632897462750927L;

	public BookNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}