package com.example.demo.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class BookIdMismatchException extends RuntimeException {
	private static final long serialVersionUID = -4225054571880447956L;

	public BookIdMismatchException(String message, Throwable cause) {
		super(message, cause);
	}
}