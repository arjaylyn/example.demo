package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.lang.NonNull;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@Entity
public class Book {
 
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private @NonNull long id;
 
	@NonNull
    @Column(nullable = false, unique = true)
    private String title;
 
    @Column(nullable = true)
	private String author;
	
}